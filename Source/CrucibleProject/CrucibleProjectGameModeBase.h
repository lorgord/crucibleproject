// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CrucibleProjectGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CRUCIBLEPROJECT_API ACrucibleProjectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
